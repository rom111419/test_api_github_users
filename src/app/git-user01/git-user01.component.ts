import { Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { GetGitHubUsersService } from '../git-user01/http.service';

export class GitUser01{
  login: string;
  id: number;
  html_url: string;
  repos_url:string;
  avatar_url:string;
}
export class GitRepos{
  id: number;
  name: string;
  html_url: string;
}

@Component({
  selector: 'app-git-user01',
  templateUrl: './git-user01.component.html',
  styleUrls: ['./git-user01.component.sass'],
  providers: [GetGitHubUsersService]
})
export class GitUser01Component implements OnDestroy {

  user: GitUser01[] = [];
  repos: GitRepos[] = [];

  private login: string;

  private subscription: Subscription;
  constructor(
      private activateRoute: ActivatedRoute,
      private getGitHubUsersService: GetGitHubUsersService
  ){
    //Следим за Логинами
    this.subscription = activateRoute.params.subscribe(
        user=>this.login=user['login']
    );

    //Юзер
    this.getGitHubUsersService.getUser(this.login)
        .subscribe(
            (user)=>this.user=user
        );

    //Вывод списка репозиториев, проектов
    this.getGitHubUsersService.getUserRepos(this.login)
        .subscribe(
            (repos)=>this.repos=repos
        );



  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }


}
