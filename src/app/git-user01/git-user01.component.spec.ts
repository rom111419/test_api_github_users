/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GitUser01Component } from './git-user01.component';

describe('GitUser01Component', () => {
  let component: GitUser01Component;
  let fixture: ComponentFixture<GitUser01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitUser01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitUser01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
