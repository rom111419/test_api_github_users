import {Injectable, Inject} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {GitUser01, GitRepos} from './git-user01.component';
import {Observable} from 'rxjs/Rx';

import 'rxjs/Rx';

@Injectable()

export class GetGitHubUsersService{
    users: GitUser01[] = [];
    repos: GitRepos[] = [];
    contrs: GitUser01[] = [];

    constructor(private _http: Http){ }

    getUsers():Observable<any> {

        //noinspection TypeScriptUnresolvedFunction
        return this._http.get('https://api.github.com/users')
            .map((resp:Response)=>{

                let usersList = resp.json();
                let users : GitUser01[] = [];
                for(let index in usersList){
                    let user = usersList[index];
                    users.push({
                        login: user.login,
                        id: user.id,
                        html_url: user.html_url,
                        repos_url: user.repos_url,
                        avatar_url: user.avatar_url
                    });
                }
                return users;
            });

    }

    getUser(user:string):Observable<any> {

        //noinspection TypeScriptUnresolvedFunction
        return this._http.get('https://api.github.com/users/' + user)
            .map((resp:Response)=>{

                let user = resp.json();
                return user;
            });

    }

    getUserRepos(user:string):Observable<any> {

        //noinspection TypeScriptUnresolvedFunction
        return this._http.get('https://api.github.com/users/' + user + "/repos")
            .map((resp:Response)=>{

                let reposList = resp.json();
                let repos : GitRepos[] = [];
                for(let index in reposList){
                    let repo = reposList[index];
                    repos.push({
                        id: repo.id,
                        name: repo.name,
                        html_url: repo.html_url,
                       });
                }
                return repos;
            });

    }

    getRepo(user:string, repo:string):Observable<any> {

        //noinspection TypeScriptUnresolvedFunction
        return this._http.get('https://api.github.com/users/' + user + "/repos" + repo )
            .map((resp:Response)=>{
                let repo = resp.json();
                return repo;

            });

    }

    //Получаем список участников проекта
    getUserReposContrs(repo:string):Observable<any> {

        //noinspection TypeScriptUnresolvedFunction
        return this._http.get('https://api.github.com/repos/defunkt/ace/contributors')
            .map((resp:Response)=>{

                let contrsList = resp.json();
                let contrs : GitUser01[] = [];
                for(let index in contrsList){
                    console.log(contrsList[index]);
                    let contr = contrsList[index];
                    contrs.push({
                        login: contr.login,
                        id: contr.id,
                        html_url: contr.html_url,
                        repos_url: contr.repos_url,
                        avatar_url: contr.avatar_url
                    });
                }
                return contrs;
            });
    }
}
