
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { MaterialModule } from '@angular/material'

import { AppComponent } from './app.component';
import { Dashboard01Component } from './dashboard01/dashboard01.component';
import { GitUsers01Component } from './git-users01/git-users01.component';
import { GitUser01Component } from './git-user01/git-user01.component';
import { GitRepo01Component } from './git-repo01/git-repo01.component';
import { GitSearchUser01Component } from './git-search-user01/git-search-user01.component';

@NgModule({
  declarations: [
    AppComponent,
    Dashboard01Component,
    GitUsers01Component,
    GitUser01Component,
    GitRepo01Component,
    GitSearchUser01Component,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    MaterialModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
