import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Dashboard01Component }   from './dashboard01/dashboard01.component';
import { GitUsers01Component }  from './git-users01/git-users01.component';
import { GitUser01Component }  from './git-user01/git-user01.component';
import { GitRepo01Component }  from './git-repo01/git-repo01.component';

const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'dashboard',  component: Dashboard01Component },
    { path: 'users',     component: GitUsers01Component },
    { path: 'user/:login', component: GitUser01Component },
    { path: 'user/:login/repo/:repoName', component: GitRepo01Component },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}


/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */