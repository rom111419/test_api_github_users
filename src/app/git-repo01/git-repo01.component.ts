import { Component, OnDestroy} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {GetGitHubUsersService} from "../git-user01/http.service";
import {GitUser01} from "../git-user01/git-user01.component";

@Component({
  selector: 'app-git-repo01',
  templateUrl: './git-repo01.component.html',
  styleUrls: ['./git-repo01.component.css'],
  providers: [GetGitHubUsersService]
})
export class GitRepo01Component implements OnDestroy {


  contrs: GitUser01[] = [];
  private repoName: string;


  private subscription: Subscription;

  constructor(
      private activateRoute: ActivatedRoute,
      private getGitHubUsersService: GetGitHubUsersService
  ){



    //Получаем название проекта
    this.subscription = activateRoute.params.subscribe(
        params=>this.repoName=params['repoName']
    );


    //Получаем список участников проекта
    this.getGitHubUsersService.getUserReposContrs(this.repoName)
        .subscribe(
            (users)=>this.contrs=users
        );
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
