/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GitRepo01Component } from './git-repo01.component';

describe('GitRepo01Component', () => {
  let component: GitRepo01Component;
  let fixture: ComponentFixture<GitRepo01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitRepo01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitRepo01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
