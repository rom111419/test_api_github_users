/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GitUsers01Component } from './git-users01.component';

describe('GitUsers01Component', () => {
  let component: GitUsers01Component;
  let fixture: ComponentFixture<GitUsers01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitUsers01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitUsers01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
