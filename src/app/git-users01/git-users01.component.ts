import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { GetGitHubUsersService } from '../git-user01/http.service';
import { GitUser01, GitRepos } from '../git-user01/git-user01.component';



@Component({
  selector: 'app-git-users01',
  templateUrl: './git-users01.component.html',
  styleUrls: ['./git-users01.component.css'],
  providers: [GetGitHubUsersService]
})
export class GitUsers01Component implements OnInit {
  users: GitUser01[] = [];

  constructor(
      private getGitHubUsersService: GetGitHubUsersService
  ) { }

  ngOnInit() {
    this.getGitHubUsersService.getUsers()
        .subscribe(
            (users)=>{
              this.users=users;
            }
        );
  }
}
