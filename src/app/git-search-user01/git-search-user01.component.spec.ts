/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GitSearchUser01Component } from './git-search-user01.component';

describe('GitSearchUser01Component', () => {
  let component: GitSearchUser01Component;
  let fixture: ComponentFixture<GitSearchUser01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitSearchUser01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitSearchUser01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
