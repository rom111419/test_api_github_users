import { Component, OnInit } from '@angular/core';
import {GitSearchUser01Service} from "./git-search-user01.service";
import {GitUser01} from "../git-user01/git-user01.component";

export class PaginationPage {
    total_count:number;
}
const PAGSPAGES:any[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];


@Component({
    selector: 'app-git-search-user01',
    templateUrl: './git-search-user01.component.html',
    styleUrls: ['./git-search-user01.component.sass'],
    providers: [GitSearchUser01Service]
})
export class GitSearchUser01Component implements OnInit {
    users:GitUser01[] = [];
    newSearchReq:string;
    pagPage:number[] = [];
    pageNumber:number;
    total_count:number;
    sort:string;
    order:string;

    constructor(private gitSearchUser01Service:GitSearchUser01Service) {

    }

    ngOnInit() {

    }

    getSearch() {
        //Pagination
        this.gitSearchUser01Service.getSearchUsersTotal_count(this.newSearchReq)
            .subscribe(
                (params)=> {
                    this.total_count = params;
                    this.pagPage = [];
                    for (let i = 1; i <= this.total_count; i++) {
                        this.pagPage.push(i);
                    }
                }
            );

        //Get users
        this.gitSearchUser01Service.getSearchUsers(
            this.newSearchReq,
            this.pageNumber,
            this.sort,
            this.order
        )
            .subscribe(
                (params)=> {
                    setTimeout(()=> {
                        this.users = params
                    }, 1000);

                }
            );
    }


}
