import {Injectable, Inject} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import {GitUser01} from "../git-user01/git-user01.component";


@Injectable()

export class GitSearchUser01Service {

  constructor(private _http: Http){ }
  getSearchUsersTotal_count(newSearchReq:string):Observable<any>{
    //noinspection TypeScriptUnresolvedFunction
    return this._http.get('https://api.github.com/search/users?q='+ newSearchReq+"&page=")
        .map((resp:Response)=>{
          let total_count = resp.json().total_count;
          total_count = Math.ceil(total_count / 30 );
          return total_count;
        });
  }

  getSearchUsers(
        newSearchReq:string,
        pageNumber:number,
        sort:string,
        order:string
  ):Observable<any> {
    //noinspection TypeScriptUnresolvedFunction
    return this._http.get('https://api.github.com/search/users?q='+ newSearchReq+"&page="+pageNumber+"&sort="+sort+"&order="+order)
        .map((resp:Response)=>{

          let usersList = resp.json().items;
          let users : GitUser01[] = [];
          for(let index in usersList){
            let user = usersList[index];
            users.push({
              login: user.login,
              id: user.id,
              html_url: user.html_url,
              repos_url: user.repos_url,
              avatar_url: user.avatar_url
            });
          }
          return users;
        });

  }

}
