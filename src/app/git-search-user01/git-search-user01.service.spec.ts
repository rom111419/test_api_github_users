/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GitSearchUser01Service } from './git-search-user01.service';

describe('GitSearchUser01Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GitSearchUser01Service]
    });
  });

  it('should ...', inject([GitSearchUser01Service], (service: GitSearchUser01Service) => {
    expect(service).toBeTruthy();
  }));
});
